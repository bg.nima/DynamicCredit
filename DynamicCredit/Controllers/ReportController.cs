﻿using DynamicCredit.Models;
using DynamicCredit.Models.Common;
using DynamicCredit.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DynamicCredit.Controllers
{
    public class ReportController : ApiController
    {
        [HttpGet]
        [Route("api/report/cpb")]
        public IEnumerable<CurrentPrincipalBalanceTableVM> CurrentPrincipalBalanceTableData([FromUri] int pageIndex, [FromUri] int itemInPage)
        {
            using (var db = new DataContext())
            {
                SqlParameter _pageIndex = new SqlParameter("@PageIndex", pageIndex);
                SqlParameter _itemInPage = new SqlParameter("@ItemInPage", itemInPage);
                return db.Database.SqlQuery<CurrentPrincipalBalanceTableVM>("current_principal_balance @PageIndex, @ItemInPage", _pageIndex, _itemInPage).ToList();
            }
        }

        [HttpGet]
        [Route("api/report/lodd")]
        public IEnumerable<LoanOriginationDateDistributionVM> LoanOriginationDateDistribution()
        {
            using (var db = new DataContext())
            {
                return db.Database.SqlQuery<LoanOriginationDateDistributionVM>("loan_origination_date_distribution").ToList();
            }
        }

        [HttpGet]
        [Route("api/report/opvd")]
        public IEnumerable<OriginalPropertyValueDistributionVM> OriginalPropertyValueDistribution()
        {
            using (var db = new DataContext())
            {
                return db.Database.SqlQuery<OriginalPropertyValueDistributionVM>("original_property_value_distribution").ToList();
            }
        }

        [HttpGet]
        [Route("api/report/iltfvd")]
        public IEnumerable<IndexedLtfvDistributionVM> IndexedLtfvDistribution()
        {
            using (var db = new DataContext())
            {
                return db.Database.SqlQuery<IndexedLtfvDistributionVM>("indexed_ltfv_distribution").ToList();
            }
        }
    }
}
