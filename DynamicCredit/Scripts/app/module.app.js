﻿(function () {
    "use strict";

    angular.module('nwise-app', [
        'ui.router',
        'ngTable'
    ]);
})();