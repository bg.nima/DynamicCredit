﻿(function () {
    "use strict";

    angular
        .module("nwise-app")
        .controller("DashboardPageController", DashboardPageController);

    DashboardPageController.$inject = ['NgTableParams', '$http'];
    function DashboardPageController(NgTableParams, $http) {
        var vm = this;

        vm.tableParams = new NgTableParams({}, {
            getData: function (params) {
                return $http.get('api/report/cpb?pageIndex=' + (params.page() - 1) + '&itemInPage=' + params.count())
                    .then(function (data) {
                        params.total(1000);
                        return data.data;
                    });
            }
        });
    }
})();