﻿(function () {
    'use strict';

    angular
        .module('nwise-app')
        .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {

            // For any unmatched url, redirect to /dashboard
            $urlRouterProvider.otherwise("/dashboard");

            $stateProvider
                .state('app', {
                    url: '',
                    templateUrl: 'Scripts/app/view.root.html',
                    //controller: 'RootController',
                    abstract: true
                })
                .state('app.dashboard', {
                    url: '/dashboard',
                    templateUrl: 'Scripts/app/pages/dashboard/view.dashboard.html',
                    controller: 'DashboardPageController',
                    controllerAs: 'vm'
                });

        }]);
})();
