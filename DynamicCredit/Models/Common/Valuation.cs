﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DynamicCredit.Models.Common
{
    public class Valuation
    {
        //public Guid M_LoanID { get; set; }
        //public int OriginalPropertyValue { get; set; }
        //public int PropertyValuationDate { get; set; }
        public double Min { get; set; }
        public double Max { get; set; }
        public double OriginalLTV { get; set; }
        public double OriginalLTFV { get; set; }
        public double IndexedLTFV { get; set; }
    }
}