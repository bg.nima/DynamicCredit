﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DynamicCredit.ViewModels
{
    public class CurrentPrincipalBalanceTableVM
    {
        public double CurrentPrincipalBalance { get; set; }
        public double OriginalPrincipalBalance { get; set; }
        public double DTI { get; set; }  
        public double LTI { get; set; }
        public double TotalIncome { get; set; }
        public decimal IndexedDTI { get; set; }
        public decimal IndexedLTI { get; set; }
        public decimal IndexedTotalIncome { get; set; }
        public double OriginalLTFV { get; set; }
        public double OriginalLTV { get; set; }
        public double OriginalForeclosureValue { get; set; }
        public double IndexedLTFV { get; set; }
        public double CurrentInterestRate { get; set; }
    }
}