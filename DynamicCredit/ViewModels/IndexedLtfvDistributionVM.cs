﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DynamicCredit.ViewModels
{
    public class IndexedLtfvDistributionVM
    {
        public double Min { get; set; }
        public double Max { get; set; }
        public double OriginalLTV { get; set; }
        public double OriginalLTFV { get; set; }
        public double OriginalPropertyValue { get; set; }
    }
}