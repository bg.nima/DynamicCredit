Dynamic Credit Assesment Project
=================

## Project Structure

Simple single page app using angularjs. all UI and .Net dependencies are provided through Nuget.
I used StructureMap as IoC container and MOQ for mocking my services.

angularjs application files are located in "~/Scripts/app" folder.

I had enough time to make changes on database for best performance and implementing required stored-procedures and made the base structure of a .net MVC project and implementing the ordered services in (ReportController).


## Change List

1. added stored procedure to retrieve paged data for current_principal_balance data table
2. added primary key to related tables in retrieving current_principal_balance data table
3. changed OriginalPropertyValue from varchar to float
4. added 3 diffrent stored procedures for 3 distribution charts data

	- indexed_ltfv_distribution
	- loan_origination_date_distribution
	- original_property_value_distribution

